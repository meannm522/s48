import React from 'react';
import CourseData from '../mockData/courseData';
import CourseCard from '../components/CourseCard';

export default function Courses() {
	//check to see if the mock data was captured
	console.log(CourseData[0]);

	//to be able to display all the couyrses from the data file, we are going to use map()
	//the "map" method loops through all the individual course object in our array and returns a component for each course
	
//Multiple components created through a map method must have a unique key that will help React JS identify which component/element have been changed, added or removed.
	const courses = CourseData.map(course => {
		return(
			<CourseCard key={course.id} courseProp={course}/>

			)
	})
	return (
		<>
			<h1>Courses</h1>
			{courses}
		</>
		)
}