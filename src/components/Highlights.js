import React from 'react';
import { Row, Col, Card } from 'react-bootstrap';


export default function Highlights() {
	return(
		
	<Row>
		<Col xs={12} md={4}>
			<Card className="cardHighlight p-3">
				<Card.Body>
					<Card.Title>
						<h2>Learn from Home</h2>
					</Card.Title>
					<Card.Text>
						Lorem ipsum dolor, sit amet consectetur adipisicing, elit. Officiis, ut quaerat, ex dolore deserunt tempora aliquid tempore, placeat odit consequuntur vitae ea accusamus fugiat accusantium nemo magnam veniam rerum laudantium.
						lorem ipsum dolor, sit amet consectetur adipisicing, elit. Officiis, ut quaerat, ex dolore deserunt tempora aliquid tempore, placeat odit consequuntur vitae ea accusamus fugiat accusantium nemo magnam veniam rerum laudantium.	
					</Card.Text>
				</Card.Body>
			</Card>
		</Col>

		<Col xs={12} md={4}>
			<Card className="cardHighlight p-3">
				<Card.Body>
					<Card.Title>
						<h2>Study Now, Pay Later</h2>
					</Card.Title>
					<Card.Text>
						Lorem ipsum dolor, sit amet consectetur adipisicing, elit. Officiis, ut quaerat, ex dolore deserunt tempora aliquid tempore, placeat odit consequuntur vitae ea accusamus fugiat accusantium nemo magnam veniam rerum laudantium.
					</Card.Text>
				</Card.Body>
			</Card>
		</Col>

		<Col xs={12} md={4}>
			<Card className="cardHighlight p-3">
				<Card.Body>
					<Card.Title>
						<h2>Be Part of Our Community</h2>
					</Card.Title>
					<Card.Text>
						Lorem ipsum dolor, sit amet consectetur adipisicing, elit. Officiis, ut quaerat, ex dolore deserunt tempora aliquid tempore, placeat odit consequuntur vitae ea accusamus fugiat accusantium nemo magnam veniam rerum laudantium.
					</Card.Text>
				</Card.Body>
			</Card>
		</Col>
	
	</Row>

	


		)
}
