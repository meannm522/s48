import React from 'react';
import './App.css';
import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import Courses from './pages/Courses';
import Register from './pages/Register';
import { Container } from 'react-bootstrap';
import Login from './pages/Login';
import Logout from './pages/Logout'
import PageNotFound from './pages/PageNotFound'

//for routes
import {BrowserRouter as Router, Routes, Route} from 'react-router-dom'

//the router(BrowserRouter?)component will enable us to simulate page navigation by synchronizing the shown content and the shown URL in the web browser

//the routes (before it call switch) declares the Route we can go to. for example
function App(){
  return (
  
    <Router>
      <AppNavbar/>
      <Container>
        <Routes>
          <Route path="/" element={ <Home />}/>
          <Route path="/courses" element={ <Courses />} />
          <Route path="/login" element={ <Login/>} />
          <Route path="/register" element={ <Register />} />
          <Route path="/logout" element={ <Logout/>} />
          <Route path="*" element={<PageNotFound/>}/>
        </Routes>
      </Container>
  
    </Router>

    );
}

export default App;


















